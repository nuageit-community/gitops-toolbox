# Semantic Versioning Changelog

## 1.0.0 (2024-01-15)


### :sparkles: News

* add code ([7472627](https://gitlab.com/nuageit-community/gitops-toolbox/commit/74726270bd49b77a6b227aa440a042a11be65c01))


### :memo: Docs

* change info ([830ab23](https://gitlab.com/nuageit-community/gitops-toolbox/commit/830ab2302af6bd341bc12b5b3f312de443419726))
