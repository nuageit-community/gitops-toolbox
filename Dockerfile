# Base image for yq binary
FROM mikefarah/yq:4.40.5 as yq

# Final base image
FROM alpine:3.19.0

# Install tools
RUN apk add --no-cache git bash

# Copy yq from yq stage
COPY --from=yq ["/usr/bin/yq", "/usr/bin/yq"]
